using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInputs : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("Mouse 0 - Left click");
            Debug.Log(Input.mousePosition);
            

            Debug.Log(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Mouse 0 - Right click");
        }
        if (Input.GetMouseButtonDown(2))
        {
            Debug.Log("Mouse 0 - Middle Mouse click");
        }
    }
}
