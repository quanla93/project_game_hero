using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject swarmerPrefabs;
    [SerializeField]
    private GameObject bigSpawnerPrefabs;

    [SerializeField]
    private float swarmerInterval = 2.5f;
    [SerializeField]
    private float bigSwarmerInterval = 10f;
    private Transform playerTransform;
    public float spawnRadius = 8f;
    public int maxEnemies = 5;
    // Start is called before the first frame update
    void Start()
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
        //StartCoroutine(spawnEnemy(swarmerInterval, swarmerPrefabs));
        //StartCoroutine(spawnEnemy(bigSwarmerInterval, bigSpawnerPrefabs));
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length < maxEnemies)
        {
            // generate a random position within the spawnRadius around the player
            Vector3 spawnPos = playerTransform.position + Random.insideUnitSphere * spawnRadius;
            spawnPos.y = 0; // set the y position to 0 to spawn the enemies on the ground

            // instantiate the enemy prefab at the random position
            GameObject enemy = Instantiate(swarmerPrefabs, spawnPos, Quaternion.identity);
            enemy.tag = "Enemy"; // set the enemy tag for future reference
        }
    }

    //private IEnumerator spawnEnemy(float interval, GameObject enemy)
    //{
    //    yield return new WaitForSeconds(interval);
    //    GameObject newEnemy = Instantiate(enemy, new Vector3(Random.Range(-5f, 5), Random.Range(-6f, 6f), 0), Quaternion.identity);
    //}
}
